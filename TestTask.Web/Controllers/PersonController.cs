﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using TestTask.Buisiness.DTO.RequestDTO;
using TestTask.Buisiness.Services.ServiceInterfaces;
using TestTask.Data.Model;

namespace TestTask.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        public readonly IPersonService personService;

        public PersonController(IPersonService personService)
        {
            this.personService = personService;
        }

        [HttpPost]
        [Route("Save")]
        public async Task<int> Save(PersonDTO person)
        {
            var savedPerson = await personService.SavePersonAsync(person);
            return Convert.ToInt32(savedPerson.Id);
        }
        [HttpPost]
        [Route("GetAll")]
        public async Task<string> GetAll(GetAllRequest request)
        {
            var people =  await personService.GetAllAsync(request);
            return JsonSerializer.Serialize(people);
        }

    }
}
