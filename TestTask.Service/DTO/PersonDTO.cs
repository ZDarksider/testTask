﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Data.Model;

namespace TestTask.Buisiness.DTO.RequestDTO
{
    public class PersonDTO
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual AddressDTO Address { get; set; }
    }
}
