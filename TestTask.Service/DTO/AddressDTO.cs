﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.Buisiness.DTO.RequestDTO
{
    public class AddressDTO
    {
        public string City { get; set; }
        public string AddressLine { get; set; }
    }
}
