﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Buisiness.DTO.RequestDTO;
using TestTask.Data.Model;

namespace TestTask.Buisiness.Services.ServiceInterfaces
{
    public interface IPersonService
    {
        public Task<Person> SavePersonAsync(PersonDTO person);
        public Task<IEnumerable<Person>> GetAllAsync(GetAllRequest getAllRequest);
    }
}
