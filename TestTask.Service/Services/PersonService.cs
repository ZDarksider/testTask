﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TestTask.Buisiness.DTO.RequestDTO;
using TestTask.Buisiness.Services.ServiceInterfaces;
using TestTask.Data.Data;
using TestTask.Data.Model;
using TestTask.Data.Repository.Base;

namespace TestTask.Buisiness.Services
{
    public class PersonService : IPersonService
    {
        public readonly IPersonRepository personRepository;

        public PersonService(IPersonRepository personRepository)
        {
            this.personRepository = personRepository;
        }

        

        public async Task<IEnumerable<Person>> GetAllAsync(GetAllRequest getAllRequest)
        {
            var allUsers = await personRepository.GetAllAsync();

            if (!String.IsNullOrWhiteSpace(getAllRequest.City))
            {
                allUsers = allUsers.Where(x => x.Address.City == getAllRequest.City);
            }
            if (!String.IsNullOrWhiteSpace(getAllRequest.FirstName))
            {
                allUsers = allUsers.Where(x => x.FirstName == getAllRequest.FirstName);
            }
            if (!String.IsNullOrWhiteSpace(getAllRequest.LastName))
            {
                allUsers = allUsers.Where(x => x.LastName == getAllRequest.LastName);
            }
            return allUsers;


        }

        public async Task<Person> SavePersonAsync(PersonDTO person)
        {
            var newPerson = new Person() { FirstName = person.FirstName, LastName = person.LastName,
                Address = new Address{ City = person.Address.City, AddressLine = person.Address.AddressLine } };
            var createdPerson = await personRepository.SavePersonAsync(newPerson);
            return createdPerson;
        }
    }
}
