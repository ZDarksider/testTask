﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Data.Data;
using TestTask.Data.Model;
using TestTask.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace TestTask.Data.Repository
{
    public class PersonRepository : IPersonRepository
    {

        public readonly TestTaskContext testTaskContext;

        public PersonRepository(TestTaskContext testTaskContext)
        {
            this.testTaskContext = testTaskContext;
        }

        public async Task<IEnumerable<Person>> GetAllAsync()
        {
           return  await testTaskContext.People.Include(x => x.Address).ToListAsync();
        }

        public async Task<Person> SavePersonAsync(Person person)
        {
           testTaskContext.Add(person);
          await  testTaskContext.SaveChangesAsync();
            return await testTaskContext.People.Where(x => x.Address == person.Address).FirstOrDefaultAsync();
        }
    }
}
