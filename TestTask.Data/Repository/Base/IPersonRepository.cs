﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TestTask.Data.Model;

namespace TestTask.Data.Repository.Base
{
    public interface IPersonRepository
    {
        public Task<Person> SavePersonAsync(Person person);
        public  Task<IEnumerable<Person>> GetAllAsync();
    }
}
