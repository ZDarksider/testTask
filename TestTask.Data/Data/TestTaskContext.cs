﻿using Microsoft.EntityFrameworkCore;
using TestTask.Data.Model;

namespace TestTask.Data.Data
{
    public class TestTaskContext : DbContext
    {
        public TestTaskContext(DbContextOptions<TestTaskContext> options) : base(options)
        {

        }

        public DbSet<Person> People { get; set; }
        public DbSet<Address> Addresses { get; set; }
    }
}
